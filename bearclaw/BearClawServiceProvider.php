<?php

namespace BearClaw;

use Illuminate\Support\ServiceProvider;

class BearClawServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton( PurchaseOrder::class, function () {
            return new PurchaseOrderService();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
