<?php
namespace BearClaw\Warehousing;

use BearClaw\Warehousing\Contracts\PurchaseOrderProduct as PurchasseOrderProductInterface;

class PurchaseOrderProduct implements PurchasseOrderProductInterface {

    private $_product = "";

    public function __construct($product)
    {
        $this->_product = $product;
    }

    /**
     * @return int
     */
    public function getProductTypeId(): int
    {
        return (int) $this->_product['product_type_id'];
    }

    /**
     * @return float
     */
    public function getUnitQuantityInitial(): float
    {
        return (float) $this->_product['unit_quantity_initial'];
    }

    /**
     * @return float
     */
    public function getVolume(): float
    {
        return (float) $this->_product['Product']['volume'];
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return (float) $this->_product['Product']['weight'];
    }
}