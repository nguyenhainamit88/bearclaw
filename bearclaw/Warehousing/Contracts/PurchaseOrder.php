<?php
namespace BearClaw\Warehousing\Contracts;

interface PurchaseOrder {

    /**
     * @param int $id
     * @return mixed
     */
    public function fetchOrder(int $id);

    /**
     * @param array $ids
     * @return array
     */
    public function calculateTotals(array $ids): array;
}