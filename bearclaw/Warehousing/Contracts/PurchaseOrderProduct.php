<?php
namespace BearClaw\Warehousing\Contracts;

interface PurchaseOrderProduct {
    /**
     * @return int
     */
    public function getProductTypeId(): int;

    /**
     * @return float
     */
    public function getUnitQuantityInitial(): float;

    /**
     * @return float
     */
    public function getWeight(): float ;

    /**
     * @return float
     */
    public function getVolume(): float;
}