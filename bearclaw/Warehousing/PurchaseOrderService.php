<?php
namespace BearClaw\Warehousing;

use \Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use \BearClaw\Warehousing\Contracts\PurchaseOrder;


class PurchaseOrderService implements PurchaseOrder
{

    const WEIGHT = 'Weight';
    const VOLUME = 'Vollume';

    /**
     * @var array
     */
    protected $prodcutTypeTotal = [];

    /**
     * @var array
     */
    protected $productTypeMethodMapping = [
        self::WEIGHT => [1,3],
        self::VOLUME => [2]
    ];

    /**
     * @param string $productType
     * @return null|string
     */
    protected function getMethod( string $productType): ?string
    {
        switch ($productType) {
            case in_array($productType, $this->productTypeMethodMapping[self::VOLUME]):
                return self::VOLUME;
            case in_array($productType, $this->productTypeMethodMapping[self::WEIGHT]):
                return self::WEIGHT;
            default:
                return '';
        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function fetchOrder(int $id)
    {
        try {
            $urlAPI = str_replace('{id}', $id, env('API_PURCHASE_ORDER_URL'));

            $fetchDetail =  Http::withBasicAuth(
                env('API_PURCHASE_ORDER_USERNAME'),
                env('API_PURCHASE_ORDER_PASSWORD'))
                ->get($urlAPI)->json();

            if ($fetchDetail['info'] === 'SUCCESS') {
                return $fetchDetail['data'];
            }

            throw new \ErrorException($fetchDetail['info']);

        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    /**
     * @return array
     */
    private function getTotal()
    {
        $result = [];

        foreach ($this->prodcutTypeTotal as $id => $total) {
            $result[] = [
                'product_type_id' => $id,
                'total' => number_format($total, 1)
            ];
        }

        return $result;
    }

    /**
     * @param PurchaseOrderProduct $purchaseOrderProduct
     */
    protected function calculate(PurchaseOrderProduct $purchaseOrderProduct)
    {
        $productTypeId = $purchaseOrderProduct->getProductTypeId();
        $unitQuantityInitial = $purchaseOrderProduct->getUnitQuantityInitial();

        if (empty($this->prodcutTypeTotal[$productTypeId])) {
            $this->prodcutTypeTotal[$productTypeId] = 0;
        }

        switch ($this->getMethod($productTypeId))
        {
            case self::WEIGHT:
                $productWeight = $purchaseOrderProduct->getWeight();
                $this->prodcutTypeTotal[$productTypeId] += $unitQuantityInitial*$productWeight;
                break;

            case self::VOLUME:
                $productWeight = $purchaseOrderProduct->getVolume();
                $this->prodcutTypeTotal[$productTypeId] += $unitQuantityInitial*$productWeight;

            default:
                //
        }

    }

    /**
     * @param array $ids
     * @return array
     */
    public function calculateTotals(array $ids): array
    {
        if (empty($ids)) {
            return [];
        }

        foreach($ids as $id) {
            $orderDetail = $this->fetchOrder($id);
            if (!empty($orderDetail['PurchaseOrderProduct'])) {
                foreach($orderDetail['PurchaseOrderProduct'] as $purchaseOrderProduct) {
                    $this->calculate(new PurchaseOrderProduct($purchaseOrderProduct));
                }
            }
        }

        return [
            "result" => $this->getTotal()
        ];
    }

}
