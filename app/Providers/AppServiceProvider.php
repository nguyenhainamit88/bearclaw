<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use BearClaw\Warehousing\Contracts\PurchaseOrder;
use BearClaw\Warehousing\PurchaseOrderService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton( PurchaseOrder::class, function () {
            return new PurchaseOrderService();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
