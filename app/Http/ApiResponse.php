<?php
namespace App\Http;

use Illuminate\Http\JsonResponse;

class ApiResponse extends JsonResponse
{
    public function __construct($payload)
    {
        if (!is_array($payload) && !is_object($payload)) {
            throw new \RuntimeException("The payload argument must be an object or an array.");
        }

        parent::__construct($payload);
    }
}
