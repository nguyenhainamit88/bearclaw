<?php

namespace App\Http\Controllers;

use App\Http\ApiResponse;
use BearClaw\Warehousing\Contracts\PurchaseOrder;
use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * @var PurchaseOrder
     */
    private $orderService;

    public function __construct(PurchaseOrder $order)
    {
        $this->orderService = $order;
    }

    public function index(Request $request)
    {
        $purchaseOrderIds = $request->get('purchase_order_ids', []);

        $total = $this->orderService->calculateTotals($purchaseOrderIds);

        return new ApiResponse($total);
    }
}
