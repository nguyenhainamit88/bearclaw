<?php

namespace Tests\Unit;

use Tests\TestCase;
use BearClaw\Warehousing\PurchaseOrderProduct;

class PurchaseOrderProductTest extends TestCase
{
    private $_product = [
        "product_type_id" => "1",
        "unit_quantity_initial" =>  "8.000",
        "Product" => [
            "product_type_id" => "1",
            "volume" => "0.500",
            "weight" => "1.500",
        ]
    ];

    private $_purchaseOrder;

    protected function setUp(): void
    {
        parent::setUp();

        $this->_purchaseOrder = new PurchaseOrderProduct($this->_product);
    }

    public function testGetProductTypeId()
    {
        $this->assertEquals(1, $this->_purchaseOrder->getProductTypeId());
    }

    public function testGetUnitQuantityInitital()
    {
        $this->assertEquals(8.0, $this->_purchaseOrder->getUnitQuantityInitial());
    }

    public function testGetVolume()
    {
        $this->assertEquals(0.5, $this->_purchaseOrder->getVolume());
    }

    public function testGetWeight()
    {
        $this->assertEquals(1.5, $this->_purchaseOrder->getWeight());
    }
}
