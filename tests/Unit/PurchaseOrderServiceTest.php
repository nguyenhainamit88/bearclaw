<?php

namespace Tests\Unit;

use Tests\TestCase;
use BearClaw\Warehousing\PurchaseOrderService;

class PurchaseOrderServiceTest extends TestCase
{
    protected $purchaseOrderService;

    public function testCalculateTotalsWithEmptyIds()
    {
        $purchaseOrderService = $this->app->make(PurchaseOrderService::class);
        $result = $purchaseOrderService->calculateTotals([]);
        $this->assertEquals([], $result);
    }

    public function testCalculateTotalsWithWorngIds()
    {
        $purchaseOrderService = $this->app->make(PurchaseOrderService::class);
        $result = $purchaseOrderService->calculateTotals([-1,-2,-3]);
        $this->assertEquals(['result' => []], $result);
    }

    public function testShouldCallFetchAPIFunction()
    {
        $this->partialMock(PurchaseOrderService::class, function ($mock) {
            $mock->shouldReceive('fetchOrder')->once();
        });
        $purchaseOrderService = $this->app->make(PurchaseOrderService::class);
        $purchaseOrderService->calculateTotals([2]);
    }

    public function testCalculateTotalsWithTheRightData()
    {
        $this->partialMock(PurchaseOrderService::class, function ($mock) {
            $mock->shouldReceive('fetchOrder')->once()->andReturn([
                "id" => "2344",
                "PurchaseOrderProduct" => [
                    [
                        "product_type_id" => "1",
                        "unit_quantity_initial" =>  "8.000",
                        "Product" => [
                            "product_type_id" => "1",
                            "volume" => "0.500",
                            "weight" => "1.500",
                        ]
                    ]
                ]
            ]);
        });
        $purchaseOrderService = $this->app->make(PurchaseOrderService::class);
        $result = $purchaseOrderService->calculateTotals([2344]);
        $this->assertEquals([
            'result' => [['product_type_id' => 1, 'total' => '12.0']]
        ], $result);
    }
}
