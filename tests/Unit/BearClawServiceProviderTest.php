<?php

namespace Tests\Unit;

use BearClaw\Warehousing\Contracts\PurchaseOrder;
use Tests\TestCase;

class BearClawServiceProviderTest extends TestCase
{
    public function testSingletonsAreInitiatedCorrectly()
    {
        $purchaseOrder = $this->app->make(PurchaseOrder::class);

        $this->assertNotNull($purchaseOrder);
        $this->assertInstanceOf(PurchaseOrder::class, $purchaseOrder);
    }
}
