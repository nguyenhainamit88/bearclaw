<?php

namespace Tests\Feature\RESTful;

use BearClaw\Warehousing\Contracts\PurchaseOrder;
use Mockery;
use Tests\TestCase;

class TotalPurchaseOrderTest extends TestCase
{
    public function testTotalPurchaseOrderWithoutAuthentication()
    {
        $response = $this->postJson('/api/test', []);

        $response->assertUnauthorized();
    }

    public function testTotalPurchaseOrderWithAuthentication()
    {
        $response = $this->postJson('/api/test',
            [],
            [
                'Authorization' => 'Basic ZGVtbzpwd2QxMjM0',
                'Content-Type' => 'application/json'
            ]);

        $response->assertSuccessful();
    }

    public function testTotalPurchaseOrderWithEmptyProductTypeIds()
    {
        $response = $this->postJson('/api/test',
            ['purchase_order_ids' => []],
            [
                'Authorization' => 'Basic ZGVtbzpwd2QxMjM0',
                'Content-Type' => 'application/json'
            ]);

        $response->assertJson([]);
    }

    public function testTotalPurchaseOrderWithProductTypeIdsWrong()
    {
        $response = $this->postJson('/api/test',
            ['purchase_order_ids' => [1]],
            [
                'Authorization' => 'Basic ZGVtbzpwd2QxMjM0',
                'Content-Type' => 'application/json'
            ]);

        $response->assertJson([]);
    }

    public function testTotalPurchaseOrderWithProductTypeIdsRight()
    {
        $this->instance(PurchaseOrder::class, Mockery::mock(PurchaseOrder::class, function ($mock) {
            $mock->shouldReceive('calculateTotals')
                ->once()
                ->with([2345])
                ->andReturn([['product_type_id' => 1, 'total' => '12.0']]);
        }));

        $response = $this->postJson('/api/test',
            ['purchase_order_ids' => [2345]],
            [
                'Authorization' => 'Basic ZGVtbzpwd2QxMjM0',
                'Content-Type' => 'application/json'
            ]);

        $response->assertExactJson([['product_type_id' => 1, 'total' => '12.0']]);
    }
}
