# Install project
```$xslt
composer install
```

# Start project
```$xslt
php artisan serve
```

# Run UnitTest
```$xslt
php artisan test
```
# Using API
Open Postman using the link: 
```
http://127.0.0.1:8000/api/test
```
In authorization tab, choose type is ```Basic Auth```. Enter username/password are: ```demo:pwd1234```